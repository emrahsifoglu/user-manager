import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Schemas } from '../common/schemas';

Schemas.InsertUser = new SimpleSchema({
    emails: {
        type: Array,
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        index: true,
        unique: true,
        max: 50,
        custom: function() {
            if (Meteor.isClient && this.isSet) {
                console.log("checking unique email");
                Meteor.call("isEmailExisting", this.value, function (error, result) {
                    if (result) {
                        console.log("Found duplicate email");   
                        Meteor.users.simpleSchema().namedContext("insertUserForm").addInvalidKeys([{name: "emails", type: "duplicateEmail"}]);
                    } else {
                        console.log("Not found duplicate email");   
                    }
                });
            }
        }
    },
    "emails.$.verified": {
        type: Boolean
    },
    password: {
        label: "Password",
        type: String,
        autoform: {
            type: 'password'
        },
        // regEx: /^(?=^.{6,}$)(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[^A-Za-z0-9]).*$/
    },
    confirmPassword: {
        label: "Confirm Password",
        type: String,
        autoform: {
            type: 'password'
        },
        custom: function () {
            if (this.value !== this.field('password').value) {
                return "passwordMismatch";
            }
        }
    },
    profile: {
        type: Schemas.Profile,
    },
    roles: {
        type: Array,
    },
    'roles.$': {
        type: String
    },
});
