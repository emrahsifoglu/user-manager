import { Schemas } from '../common/schemas';

Schemas.Profile = new SimpleSchema({
    firstName: {
        label: "First Name",
        type: String,
        regEx: /^[a-zA-Z]{2,25}$/,
    },
    lastName: {
        label: "Last Name",
        type: String,
        regEx: /^[a-zA-Z]{2,25}$/,
    },
    birthDate: {
        type: Date,
    },
    age: {
        type: SimpleSchema.Integer,
    },
});
