import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Schemas } from '../common/schemas';

Schemas.User = new SimpleSchema({
    emails: {
        type: Array,
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        index: true,
        unique: true,
        max: 50,
        custom: function() {
            if (Meteor.isClient && this.isSet) {
                console.log("checking unique email");
                Meteor.call("isEmailExisting", this.value, function (error, result) {
                    if (result) {
                        console.log("Found duplicate email");   
                        Meteor.users.simpleSchema().namedContext("insertUserForm").addInvalidKeys([{name: "emails", type: "duplicateEmail"}]);
                    } else {
                        console.log("Not found duplicate email");   
                    }
                });
            }
        }
    },
    "emails.$.verified": {
        type: Boolean
    },
    profile: {
        type: Schemas.Profile,
    },
    roles: {
        type: Array,
    },
    'roles.$': {
        type: String
    },
    /*
    createdAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date;
            } else if (this.isUpsert) {
                return {$setOnInsert: new Date};
            } else {
                this.unset();
            }
        }
    },
    */
});
