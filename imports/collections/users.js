import { Meteor } from 'meteor/meteor';
import { Schemas } from '../common/schemas';
import Profile from '../schemas/profile';
import User from '../schemas/user';
import InsertUser from '../schemas/insertUser';

Meteor.users.attachSchema(Schemas.User);
