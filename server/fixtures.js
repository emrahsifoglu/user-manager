import faker from 'faker';

Meteor.startup(function() {
    if (Meteor.users.find().fetch().length === 0) {
        console.log('Creating users...');

        var usersData = [{
            email:'admin@example.com',
            password: '123456',
            roles:['admin'],
            profile: {
                firstName:'Admin',
                lastName: 'User',
                birthDate: new Date(moment().startOf('month').subtract(33, 'y')),
                age: 33
            }},
        ];

        _.each(_.range(9), function() {
            var email = faker.internet.email();
            var password = faker.internet.password();
            var firstName = faker.name.firstName();
            var lastName = faker.name.lastName();
            var age = faker.random.number({'min': 18, 'max': 35}); 
            var birthDate = new Date(moment().startOf('month').subtract(age, 'y'));
        
            var userData = {
                email: email,
                password: '123456',
                roles:['normal'],
                profile: {
                    firstName: firstName,
                    lastName: lastName,
                    birthDate: birthDate,
                    age: age
                },
            };

            usersData.push(userData);
        });

        _.each(usersData, function(userData) {
            console.log(userData);

            id = Accounts.createUser(userData);
            Meteor.users.update({_id: id}, {$set:{'emails.0.verified': true}});
            Roles.addUsersToRoles(id, userData.roles);
        });
    }
});
