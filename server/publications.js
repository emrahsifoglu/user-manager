if (Meteor.isServer) {
    Meteor.publish("users", function() {
        var user = Meteor.users.findOne({_id:this.userId});

        if (Roles.userIsInRole(user, ['admin'])) {
            console.log('publishing users', this.userId)

            return Meteor.users.find({}, {fields: {emails: 1, profile: 1, roles: 1}});
        }

        this.stop();
        return;
    });
}
