import { AccountsTemplates } from 'meteor/useraccounts:core';

function postSignUpHook(userId, info) {
  Meteor.users.update({_id: userId}, {$set:{'emails.0.verified': true}});
  Roles.addUsersToRoles(userId, ['normal']);
}

AccountsTemplates.configure({
  postSignUpHook: postSignUpHook,
});    
