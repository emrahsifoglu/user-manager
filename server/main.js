import { Meteor } from 'meteor/meteor';

if (Meteor.isServer) {
    Meteor.startup(() => {
        console.log('Server is starting up...');

        Meteor.users.allow({
            insert: function(userId, doc) {
                var loggedInUser = Meteor.user();  

                return Roles.userIsInRole(loggedInUser, ['admin']);
            },
            update: function(userId, doc) {
                var loggedInUser = Meteor.user();

                return loggedInUser._id === doc._id || Roles.userIsInRole(loggedInUser, ['admin']);
            },
        });

        Meteor.methods({
            testMethod() {
                console.log('test');
            },
            isEmailExisting: function(emailToCheck) {
                var count = Meteor.users.find({'emails.address': emailToCheck}).count();
                console.log("Found emails-" + count);

                return count > 0;
            },
            insertUser: function(doc) {
                var userData = {
                    email: doc.emails[0].address,
                    password: doc.password,
                    roles: doc.roles,
                    profile: doc.profile,
                };

                id = Accounts.createUser(userData);
                Meteor.users.update({_id: id}, {$set:{'emails': doc.emails}});
                Roles.addUsersToRoles(id, userData.roles);

                return true;
            },
            removeUser(user) {
                var loggedInUser = Meteor.user();
                if (loggedInUser._id !== user._id && Roles.userIsInRole(loggedInUser, ['admin']) && !Roles.userIsInRole(user, ['admin'])) {
                    Meteor.users.remove({ _id: user._id }, function (error, result) {
                        if (error) {
                            throw new Meteor.Error('UserRemoveError', error);
                        }
                    }); 
                } else {
                    throw new Meteor.Error('UserRemoveError', 'Error removing user: self or admin');
                }
            },
            removeAllUsers() {
                return Meteor.users.remove({});
            },
        });
    });
}
