import { Template } from 'meteor/templating';
import { Schemas } from '../imports/common/schemas';
import Users from '../imports/collections/users';

Template.registerHelper("Schemas", Schemas);

Template['override-atSigninLink'].replaces('atSigninLink');
Template['override-atSignupLink'].replaces('atSignupLink');
Template['override-atPwdFormBtn'].replaces('atPwdFormBtn');
Template['override-atNavButton'].replaces('atNavButton');
Template['override-atTitle'].replaces('atTitle');

Template.atForm.rendered = function() {
    $('#at-field-birthDate').each(function() {
        $(this).datetimepicker({
            format: 'DD.MM.YYYY',
        });
    });
}

Template.userList.onCreated(function() {
    this.subscribe("users");
});

Template.userList.helpers({
    users: function() {
        return Meteor.users.find();
    },
});

Template.user.helpers({
    firstName: function() {
        if (!this.profile || !this.profile.firstName) {
             return '<none>';
        }

        return this.profile.firstName;
    },
    lastName: function() {
        if (!this.profile || !this.profile.lastName) {
             return '<none>';
        }

        return this.profile.lastName;
    },
    roles: function() {
        if (!this.roles) {
            return '<none>'
        }

        return this.roles.join(',')
    },
    email: function() {
        return this.emails[0].address;
    },
    age: function() {
        if (!this.profile || !this.profile.age) {
            return '<none>';
        }

        return this.profile.age;
    },
    birthDate: function() {
        if (!this.profile || !this.profile.birthDate) {
            return '<none>';
        }

        return moment(this.profile.birthDate).format('DD.MM.YYYY');
    },
});

Template.user.events({
    'click .delete-user': function() {
        try {
            Meteor.call('removeUser', this);
          } catch (e) {
            console.log(e);  
        }
    }
});

Template.nav.events({
	'click .logout': function(event){
		event.preventDefault();
		AccountsTemplates.logout();
	}
});

Template.nav.helpers({
    fullName: function() {
        var user =  Meteor.user();
        if (!user.profile || !user.profile.firstName || !user.profile.lastName) {
             return '<none>';
        }

        return user.profile.firstName + ' ' + user.profile.lastName;
    },
    email: function() {
        var user =  Meteor.user();

        return user.emails[0].address;
    },
});

Template.updateUserForm.helpers({
    user: function() {
        return Meteor.user();
    },
});

Template.adminUserUpdate.onCreated(function() {
    this.params = this.data.params();
});
 
Template.adminUserUpdate.helpers({
    userId: function() {
        return Template.instance().params.userId;
    },
    user: function() {
        return Meteor.users.findOne({_id: this.userId}) || {};
    }
});

AutoForm.hooks({
    insertUserForm: {
        onSubmit: function(insertDoc, updateDoc, currentDoc) {
            Meteor.call('insertUser', insertDoc);
            this.done();
            return false;
        },before: function () {
            // show loader
        },
        after: function() {
            // hide loader
        }
    },
});
