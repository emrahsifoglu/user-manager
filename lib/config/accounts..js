import { AccountsTemplates } from 'meteor/useraccounts:core';

var email = AccountsTemplates.removeField('email');
var pwd = AccountsTemplates.removeField('password');

/*
{
  _id: 'age',
  type: 'text',
  displayName: 'Age',
  re: /^([1-9][0-9]?){0,1}$/,
  errStr: 'Please enter your age between 1-99.',
  placeholder: 'Age',
  required: true,
}
*/

AccountsTemplates.addFields([{
  _id: 'firstName',
  type: 'text',
  displayName: 'First name',
  re: /^[^\d]{2,}$/i,
  errStr: 'Please enter your first name.',
  placeholder: 'First name',
  required: true,
},{
  _id: 'lastName',
  type: 'text',
  displayName: 'Last name',
  re: /^[^\d]{2,}$/i,
  errStr: 'Please enter your last name.',
  placeholder: 'Last name',
  required: true,
},{
  _id: 'birthDate',
  type: 'text',
  displayName: 'Birthdate',
  re: /^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$/g,
  errStr: 'Please enter your birthdate in format DD.MM.YYYY',
  placeholder: 'DD.MM.YYYY',
  required: true,
}]);

AccountsTemplates.addField(email);
AccountsTemplates.addField(pwd);

AccountsTemplates.configure({
  defaultLayout: 'masterLayout',
  defaultLayoutRegions: {
    nav: 'nav',
    footer: 'footer',
  },
  defaultContentRegion: 'main',
  showForgotPasswordLink: false,
  overrideLoginErrors: true,
  enablePasswordChange: false,
  showLabels: true,
  showPlaceholders: true,
  negativeValidation: true,
  positiveValidation: true,
  negativeFeedback: true,
  positiveFeedback: true,
});

AccountsTemplates.configureRoute('signIn', {
  name: 'login',
  path: '/login',
});

AccountsTemplates.configureRoute('signUp', {
  name: 'register',
  path: '/register',
});
