function requireRoleAdmin(context,redirect) {
  if (Meteor.isClient){
    if (!Roles.userIsInRole(Meteor.userId(), ['admin'])) {
      redirect('/');
    }
  }
}

FlowRouter.route('/', {
  name: "home",
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: "footer",
      main: "home",
      nav: "nav",
    });
  }
});

FlowRouter.route('/admin', {
  name: "admin",
  triggersEnter: [AccountsTemplates.ensureSignedIn, requireRoleAdmin],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: "footer",
      main: "admin",
      nav: "nav",
    });
  }
});

FlowRouter.route('/admin/user/insert', {
  name: "admin.user.insert",
  triggersEnter: [AccountsTemplates.ensureSignedIn, requireRoleAdmin],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: "footer",
      main: "admin.user.insert",
      nav: "nav",
    });
  }
});

FlowRouter.route('/admin/user/update/:userId', {
  name: "admin.user.update",
  triggersEnter: [AccountsTemplates.ensureSignedIn, requireRoleAdmin],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: "footer",
      main: "adminUserUpdate",
      nav: "nav",
      params: params,
    });
  }
});

FlowRouter.route('/user/update', {
  name: "user.update",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: "footer",
      main: "user.update",
      nav: "nav",
    });
  }
});

FlowRouter.notFound = {
  action: function() {
    BlazeLayout.render('masterLayout', {
      footer: "footer",
      main: "pageNotFound",
      nav: "nav",
    });
  }
};

FlowRouter.route('/remove/user/all', {
  name: "removeAllUsers",
  action: function(params, queryParams) {
    Meteor.call('removeAllUsers');
    FlowRouter.go('/');
  }
});
